import requests
from http import HTTPStatus
import abc 
import attr

class Requester(abc.ABC):

    @abc.abstractmethod
    def get(self):
        raise NotImplementedError


attr.s(auto_attribs=True)
class RequestsRequester(Requester):
    
    url: str = 'https://shipping-options-api.herokuapp.com/v1/shipping_options'
    
    def get(self):
        return requests.get(self.url)


class Api():
    def __init__(self, requester: Requester):
        self.requester = requester 
    
    def get(self):
        options = self.requester.get().json()['shipping_options'] 
        sorted_options = sorted(options, key=lambda opt: (opt['cost'],opt['estimated_days']) )
        return sorted_options, HTTPStatus.OK
