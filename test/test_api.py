import pytest
from ej import Api
from unittest import mock
import json
@pytest.fixture(name='case1')
def fixture_case_1():
    return (
        [
            {"name":"Option 1","type":"Delivery","cost":10,"estimated_days":3},
            {"name":"Option 2","type":"Custom","cost":10,"estimated_days":3},
            {"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3}
        ],
        [
            {"name":"Option 1","type":"Delivery","cost":10,"estimated_days":3},
            {"name":"Option 2","type":"Custom","cost":10,"estimated_days":3},
            {"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3}
        ]
    )

@pytest.fixture(name='case2')
def fixture_case_2():
    return (
        [
            {"name":"Option 1","type":"Delivery","cost":10,"estimated_days":5},
            {"name":"Option 2","type":"Custom","cost":10,"estimated_days":2},
            {"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3}
        ],
        [
            {"name":"Option 2","type":"Custom","cost":10,"estimated_days":2},
            {"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3},
            {"name":"Option 1","type":"Delivery","cost":10,"estimated_days":5}]
    )


@pytest.fixture(name='case3')
def fixture_case_3():
    return (
        [
            {"name":"Option 1","type":"Delivery","cost":6,"estimated_days":3},
            {"name":"Option 2","type":"Custom","cost":5,"estimated_days":3},
            {"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3}
            ],
        [
            {"name":"Option 2","type":"Custom","cost":5,"estimated_days":3},
            {"name":"Option 1","type":"Delivery","cost":6,"estimated_days":3},
            {"name":"Option 3","type":"Pickup","cost":10,"estimated_days":3}
        ]
    )


@pytest.fixture(name='case4')
def fixture_case_4():
    return (
        [
            {"name":"Option 1","type":"Delivery","cost":10,"estimated_days":5},
            {"name":"Option 2","type":"Custom","cost":5,"estimated_days":3},
            {"name":"Option 3","type":"Pickup","cost":7,"estimated_days":2}
        ],
        [
            {"name":"Option 2","type":"Custom","cost":5,"estimated_days":3},
            {"name":"Option 3","type":"Pickup","cost":7,"estimated_days":2},
            {"name":"Option 1","type":"Delivery","cost":10,"estimated_days":5}
        ]
    )

@pytest.fixture(name='case5')
def fixture_case_5():
    return (
        [],
        []
    )

@pytest.fixture(name='requester_factory')
def fixture_requester():

    def factory(case):
        mock_response = mock.MagicMock()
        mock_response.json.return_value = {'shipping_options':case[0]}
        requester = mock.MagicMock()
        
        requester.get.return_value = mock_response
        
        return requester
    return factory

def test_case1(requester_factory, case1):

    api = Api(requester=requester_factory(case1))
    assert api.get()[0] == case1[1]

def test_case2(requester_factory, case2):

    api = Api(requester=requester_factory(case2))
    assert api.get()[0] == case2[1]

def test_case3(requester_factory, case3):

    api = Api(requester=requester_factory(case3))
    assert api.get()[0] == case3[1]


def test_case4(requester_factory, case4):

    api = Api(requester=requester_factory(case4))
    assert api.get()[0] == case4[1]

def test_case5(requester_factory, case5):

    api = Api(requester=requester_factory(case5))
    assert api.get()[0] == case5[1]